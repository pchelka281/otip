# OTIP

## FastAPI Todo LR1

Installation with Docker
```bash
sudo docker build -t 2020-3-00-lr1 .
sudo docker run --rm -p 8000:8000 2020-3-00-lr1

# with database save
sudo docker run --rm -p 8000:8000 -v "${PWD}/data/":/code/data/ 2020-3-00-lr1

# development
sudo docker run --rm -p 8000:8000 -v "${PWD}/app":/code/app -v "${PWD}/data/":/code/data/ 2020-3-00-lr1

# generate
sudo docker build -t 2020-3-00-lr1-generate -f Dockerfile_generate .
sudo docker run --rm --network=host 2020-3-00-lr1-generate
```

---

## LR1

0. Download the project using the terminal or web. Below is the command for downloading via the terminal.

```bash
git clone git@gitlab.com:pchelka281/otip.git
```

1. Go to the project directory and run the docker compose file.

```bash
# Go to the project directory
cd otip

# Run the compose file
sudo docker compose up
```

Important! Before running, make sure that enough memory is allocated in the system. To do this, run the following command:

```bash
docker logs elasticsearch -f
    
# Fix as needed
sysctl -w vm.max_map_count=262144
```

2. After everything is up and running, check the operation at http://localhost:9200/ and http://localhost:5601/

3. If further work with the project is needed, it is recommended to read the book "Elasticsearch, Kibana, Logstash, and Next-Generation Search Systems (2019)"

Note! For the initial setup, it is necessary to create a cluster. Therefore, deploy this project on at least two machines from the same subnet. Specify the machine addresses in a separate field in the docker-compose.yml file (currently located at: ./otip/lr-1/)

---

## LR2

### Before Running

* Download this repository (before downloading, check the presence of your public key in the account linked to the project):

```bash
git clone git@gitlab.com:pchelka281/otip.git
```

---

### Running

* Go to the project directory and run the compose file. The sequence of commands:

```bash
# Don't forget about memory
sysctl -w vm.max_map_count=262144

# Go to the downloaded project
cd otip

# Run the compose file
sudo docker compose up
```

* After the necessary data is fetched, go to the browser and enter the address to work with the to-do app (http://localhost:8000)

### Usage

* Here, you can add tasks that will be stored in Elasticsearch. To do this, enter the task in the corresponding field, select one of the suggested tags, and click the "Add" button. You will immediately see the new entry below.
 
* You can edit the records. You can change the tag, task text, and mark it as completed. Additionally, you can attach a document to the task, and its contents will also be stored in the database.

* File deletion occurs by clicking the corresponding button. After deletion, all task records will be removed.

* Additionally, there is a search function for tasks based on text, date, and tag.
---

### Generating 20 Tasks

* In the "scripts" directory, there is a file for generating twenty tasks. You can run it directly on your device.

---

## LR3

https://gitlab.com/dejitarudemon1/otip