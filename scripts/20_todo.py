import random
import requests
from elasticsearch import Elasticsearch
from models import Todo
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

elastic = Elasticsearch('http://localhost:9200/')

index_name = "todo_marta"
url = 'http://localhost:8000/add/'
response = requests.get('http://localhost:8000/')
titles = [
    'Task 1',
    'Task 2',
    'Task 3',
    'Task 4',
    'Task 5',
    'Task 6',
    'Task 7',
    'Task 8',
    'Task 9',
    'Task 10',
    'Task 11',
    'Task 12',
    'Task 13',
    'Task 14',
    'Task 15',
    'Task 16',
    'Task 17',
    'Task 18',
    'Task 19',
    'Task 20'
]
DB_PATH = "/data01/"
DB_URL = f"sqlite:///{DB_PATH}/db.sqlite"
engine = create_engine(DB_URL)
Session = sessionmaker(bind=engine)
session = Session()

for i in range(20):
    task = titles[i]
    todo = Todo(task=task)
    session.add(todo)
    session.commit()
    response = elastic.index(index=index_name, id=todo.id, doc_type='todo', body=todo.get_dict())

session.close()
