import models
from elasticsearch import Elasticsearch
import datetime
from loguru import logger

elastic = Elasticsearch(['http://elasticsearch:9200'])
index = 'todo_p4el'
logger = logger.opt(colors=True)


async def add_todo(todo: models.Todo):
    body = {
        'task': todo.task,
        'completed': todo.completed,
        'tag': todo.tag,
        'date': datetime.date.today().isoformat()
    }
    elastic.index(index=index, doc_type='todo', id=todo.id, body=body)


async def file_todo(todo_id: int, file_path: str, filename: str):
    with open(file_path, 'r') as file:
        file_data = file.read()
    body = {
        'doc': {
            'filename': filename,
            'file_text': file_data
        }
    }

    elastic.index(index=index, doc_type='todo', id=todo_id, body=body)


async def search_todo(query: str):
    # Поиск задач по введенной строке в Elasticsearch
    body = {
        'query': {
            "bool": {
                "should": [
                    {"match": {"task": query}},
                    {"match": {"doc.file_text": query}}
                ]
            }
        }
    }
    res = elastic.search(index=index, body=body)
    hits = res['hits']['hits']

    # Отображение найденных задач
    result = []
    for hit in hits:
        source = hit['_source']
        logger.info(f"AAA: {source=}")
        todo = models.Todo(id=hit['_id'],
                           task=source['task'],
                           completed=source['completed'],
                           tag=source['tag'])
        result.append(todo)

    return result

async def search_date_todo(query: str):
    # Поиск задач по конкретной дате в Elasticsearch
    search_date = datetime.datetime.strptime(query, '%Y-%m-%d').date()
    body = {
        'query': {
            'bool': {
                'must': [
                    {
                        'term': {
                            'date': search_date.isoformat()
                        }
                    }
                ]
            }
        }
    }

    res = elastic.search(index=index, body=body)
    hits = res['hits']['hits']

    # Отображение найденных задач
    result = []
    for hit in hits:
        source = hit['_source']
        todo = models.Todo(id=hit['_id'],
                           task=source['task'],
                           completed=source['completed'],
                           tag=source['tag'])
        result.append(todo)

    return result



async def search_tag_todo(query: str):
    # Поиск задач по введенной строке в Elasticsearch

    body = \
        {
            'query': {
                "match": {
                    "tag": query
                }
            }
        }

    res = elastic.search(index=index, body=body)
    hits = res['hits']['hits']

    # Отображение найденных задач
    result = []
    for hit in hits:
        source = hit['_source']
        todo = models.Todo(id=hit['_id'],
                           task=source['task'],
                           completed=source['completed'],
                           tag=source['tag'])
        result.append(todo)

    return result


async def edit_todo(todo: models.Todo, todo_id: int):
    elastic.update(index=index, doc_type='todo', id=todo_id,
                   body={"doc": {'task': todo.task, "completed": todo.completed,
                                 "tag": todo.tag, "date": datetime.date.today().isoformat()}})


async def delete_todo(todo_id: int):
    elastic.delete(index=index, doc_type='todo', id=todo_id)
