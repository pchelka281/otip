FROM python:3.10-slim

COPY requirements.txt /
ARG PROXY

RUN  pip install --upgrade pip

RUN if [ -z "$PROXY" ]; then \
    pip3 install --no-cache-dir -r /requirements.txt; \
    else \
    pip install --proxy "$PROXY" -r /requirements.txt; \
    fi

RUN if [ -z "$PROXY" ]; then \
    pip install --no-cache-dir --upgrade urllib3; \
    else \
    pip install --proxy "$PROXY" --upgrade urllib3; \
    fi

COPY app /code/app
WORKDIR /code/app

ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]
